java -jar PATH_TO_PASSBIRD_UPDATER_JAR_DIRECTORY/passbird-updater.jar PATH_TO_PASSBIRD_JAR_DIRECTORY
java -jar PATH_TO_PASSBIRD_JAR_DIRECTORY/passbird.jar PATH_TO_PASSBIRD_HOME

# example with the following assumptions:
#
# - passbird-updater.jar located in /opt/passbird-updater/
# - passbird.jar located in /opt/passbird/
# - passbird home located in /home/user/passbird/
#
# java -jar /opt/passbird-updater/passbird-updater.jar /opt/passbird/
# java -jar /opt/passbird/passbird.jar /home/user/passbird/
